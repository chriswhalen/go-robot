Rails.application.routes.draw do

	devise_for :users, controllers: {registrations: "users/registrations"}

	get '/event/:left/:right', to: 'application#event'
	get '/events', to: 'application#all_events'
	get '/bet/:side', to: 'application#bet'
	get '/bets', to: 'application#bets'

	root to: 'application#index'

end

