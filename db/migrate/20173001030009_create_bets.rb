class CreateBets < ActiveRecord::Migration[5.1]
  def change
    create_table :bets do |t|
      t.string :side
      t.integer :user_id
      t.timestamps
    end
  end
end
