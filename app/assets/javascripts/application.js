// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .

$(function(){


	var left_handler = StripeCheckout.configure({
	  key: 'pk_test_p3VYB9OTH2F7L4bHt3RLTW5R',
	  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
	  locale: 'auto',
	  token: function(token) {
	    $.get('/bet/left', '', doBets)
	  }
	});

	var right_handler = StripeCheckout.configure({
	  key: 'pk_test_p3VYB9OTH2F7L4bHt3RLTW5R',
	  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
	  locale: 'auto',
	  token: function(token) {
	    $.get('/bet/right', '', doBets)
	  }
	});

	$('#left-bet').click( function(e) {
	  // Open Checkout with further options:
	  left_handler.open({
	    name: 'Go Robot',
	    description: '1 bet for Ninjas in Pyjamas',
	    currency: 'cad',
	    amount: 500
	  });
	  e.preventDefault();
	});

	$('#right-bet').click( function(e) {
	  // Open Checkout with further options:
	  right_handler.open({
	    name: 'Go Robot',
	    description: '1 bet for FaZe Clan',
	    currency: 'cad',
	    amount: 500
	  });
	  e.preventDefault();
	});

	// Close Checkout on page navigation:
	window.addEventListener('popstate', function() {
	  handler.close();
	});

	setInterval(function(){
		doBets()
		doEvents()
	}, 4000)

	doBets = function() {
		$.get('/bets', '', function(data){
			$('#left-bets').text(data[0] + ' bets')
			$('#right-bets').text(data[1] + ' bets')
		})
	}

	doEvents = function() {
		$.get('/events', '', function(data){

			$.each(data, function(e, i){

				$('#events').prepend('<li> <h3>'+i.left+' &#9760; '+i.right+'</h3> </li>')

			})
			
		})
	}

	doBets()
	doEvents()

})