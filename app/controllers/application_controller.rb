class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception

	def index
	end

	def event
		event = Event.new
		event.left = params[:left]
		event.right = params[:right]
		event.save
		return render :json => event
	end

	def all_events
		return render :json => Event.fresh
	end

	def bet
		bet = Bet.new
		bet.side = params[:side]
		bet.save
		return render :json => bet
	end

	def bets
		return render :json =>[Bet.left.count, Bet.right.count]
	end

end
