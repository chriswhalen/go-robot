class Event < ApplicationRecord
  
	scope :fresh, -> { where 'updated_at > ?', 4.seconds.ago }

end
