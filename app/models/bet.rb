class Bet < ApplicationRecord
  
	scope :left, -> { where(side: 'left') }
	scope :right, -> { where(side: 'right') }

end
