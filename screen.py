from __future__ import division, print_function
from fuzzywuzzy import fuzz, process
from PIL import Image, ImageOps, ImageEnhance
from pprint import pprint as pp
import cv2
import fuzzywuzzy
import mss
import numpy as np
import pytesseract
import datetime
import threading
import time
import requests

def ocr( image ):

    ocr_result = pytesseract.image_to_string(image, config='--psm 7')
    spaces = [ c for c in ocr_result if c == " " ]
    newlines = [ c for c in ocr_result if c == "\n" ]

    print(ocr_result)

    if len(ocr_result) > 11 and len(spaces) / len(ocr_result) < 0.4 and len(newlines) < 1:

        result = [0,0]

        for team in teams:

            players = []
            for p in teams[team]['players']:
                if teams[team]['tag']:
                    players.append( teams[team]['tag'] +' '+ p )
                else:
                    players.append( p )

            if ocr_result.find(' + '):
                ocr_result.find(' + ')
            else:
                split_i = int(len(ocr_result)/2)
            
            ps = ( ocr_result[:int(len(ocr_result)/2)], ocr_result[int(len(ocr_result)/2):] )
            guesses = ( process.extractOne(ps[0], players), process.extractOne(ps[1], players) )
            
            if guesses[0][1] > 54 or guesses[1][1] > 54:
                if guesses[0][1] > guesses[1][1]:
                    result[0] = [guesses[0][0]]
                else:
                    result[1] = [guesses[1][0]]

        if result[0] and result[1]:

            r = requests.get('http://go-robot.herokuapp.com/event/'+ str(result[0][0]) + '/' + str(result[1][0]))
            print( str(r) )


def parse(i, image): 
 
    enhancer = ImageEnhance.Sharpness(image)
    image = enhancer.enhance(72)
    
    enhancer = ImageEnhance.Contrast(image)
    image = enhancer.enhance(1.8)
    
    enhancer = ImageEnhance.Brightness(image)
    image = enhancer.enhance(0.0048)

    image = np.array(image)
    image = cv2.medianBlur(image,17)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    r, thresh = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU)

    image = ImageOps.invert(Image.fromarray(thresh))

    # ocr(image)
    t = threading.Thread(target=ocr, args=(image,))
    return t.start()


def trim(frame):
    #crop top
    if not np.sum(frame[0]):
        return trim(frame[1:])
    #crop bottom
    elif not np.sum(frame[-1]):
        return trim(frame[:-2])
    #crop left
    elif not np.sum(frame[:,0]):
        return trim(frame[:,1:]) 
    #crop right
    elif not np.sum(frame[:,-1]):
        return trim(frame[:,:-2])    
    return frame

teams = {

    'Ninjas in Pyjamas': {
        'tag': 'NiP',
        'players': ['draken', 'f0rest', 'GeT_RiGhT', 'REZ', 'THREAT', 'Xizt']
    },
    'FaZe Clan': {
        'tag': 'FaZe',
        'players': ['GuardiaN', 'karrigan', 'niko', 'olofmeister', 'rain', 'ROBBANOWSKI']
    }
}

monitor = {'top': 0, 'left': 0, 'width': 2560, 'height': 3200}

for loop in range(1, 2048):

    cap = np.array(mss.mss().grab(monitor))[::2]

    log_images = [
        Image.fromarray(cap[170:218, -641:-1]).resize((4800, 360), resample=Image.LANCZOS),
        Image.fromarray(cap[218:266, -641:-1]).resize((4800, 360), resample=Image.LANCZOS),
        Image.fromarray(cap[266:314, -641:-1]).resize((4800, 360), resample=Image.LANCZOS),
        Image.fromarray(cap[314:362, -641:-1]).resize((4800, 360), resample=Image.LANCZOS),
        Image.fromarray(cap[362:410, -641:-1]).resize((4800, 360), resample=Image.LANCZOS),
    ]

    score = Image.fromarray(cap[0:100, 400:600]).resize((2000, 1000), resample=Image.LANCZOS),

    # score.

    threads = []
    for i, image in enumerate(log_images):
        t = threading.Thread(target=parse, args=(i, image))
        threads.append(t)
        t.start()

    time.sleep(2)

